package com.example.todo

import android.content.Intent
import android.os.Bundle
import android.util.Log.d
import android.view.View
import android.widget.RadioButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.todo.user.UserInformation
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    var databaseF = FirebaseDatabase.getInstance().reference
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        init()
    }

    private fun init() {
        auth = Firebase.auth
    }

    fun signUp(view: View) {
        val name: String = name_edit_text.text.toString()
        val lastName: String = last_name_edit_text.text.toString()
        val profession: String = profession_edit_text.text.toString()
        val email: String = sign_up_email_edit_text.text.toString()
        val password: String = sign_up_password_edit_text.text.toString()
        val repeatPassword: String = sign_up_repeat_password_edit_text.text.toString()
        if (email.isNotEmpty() && password.isNotEmpty() && repeatPassword.isNotEmpty() && name.isNotEmpty() && lastName.isNotEmpty() && profession.isNotEmpty()) {
            if (password == repeatPassword) {
                sign_up_progress_bar.visibility = View.VISIBLE
                auth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this) { task ->

                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            d("signUp", "createUserWithEmail:success")
                            val user = auth.currentUser
                            openLogIn()
                            saveUserData()
                        } else {
                            // If sign in fails, display a message to the user.
                            d("signUp", "createUserWithEmail:failure", task.exception)
                            Toast.makeText(
                                baseContext, "Authentication failed." + task.exception,
                                Toast.LENGTH_SHORT
                            ).show()
                        }

                    }
            } else {
                Toast.makeText(this, "Please Repeat your password!!", Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(this, "Please fill all field!!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun openLogIn() {
        val intent = Intent(this, LogInActivity::class.java)
        startActivity(intent)
        overridePendingTransition(
            android.R.anim.fade_in, android.R.anim.fade_out
        )
        sign_up_progress_bar.visibility = View.GONE

    }

    private fun saveUserData() {
        val userID = auth.currentUser?.uid
        val name = name_edit_text.text.toString()
        val last_name = last_name_edit_text.text.toString()
        val prof = profession_edit_text.text.toString()
        val Email = sign_up_email_edit_text.text.toString()
        val mobileNum = sign_up_mobile_num_edit_text.text.toString()
        val passWord = sign_up_repeat_password_edit_text.text.toString()

        val selectedRDBT = radio_gro.checkedRadioButtonId
        val radio = findViewById<RadioButton>(selectedRDBT)
        val radioResult = radio.text.toString()
        databaseF.child("User").child(userID.toString()).setValue(
            UserInformation(
                name,
                last_name,
                prof,
                Email,
                mobileNum,
                passWord,
                radioResult
            )
        )

    }
}