package com.example.todo

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.setupActionBarWithNavController
import com.bumptech.glide.Glide
import com.example.todo.fragments.ProfileFragmentDirections
import com.example.todo.viewPager.ViewPagerFragmentDirections
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.nav_head.*

class MainActivity : AppCompatActivity() {

    lateinit var toggle: ActionBarDrawerToggle
    private lateinit var auth: FirebaseAuth
    var databaseG = FirebaseDatabase.getInstance().reference
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        auth = Firebase.auth
        supportActionBar?.elevation = 0F
//        setupActionBarWithNavController(findNavController(R.id.fragment))
        drawer()
        getData()
    }
    private fun drawer() {
        toggle = ActionBarDrawerToggle(this, drawer_layout, R.string.open, R.string.close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        nav_drawer.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.menu_main -> {
                    val action =
                        ProfileFragmentDirections.actionProfileFragmentToViewPagerFragment()
                    findNavController(R.id.fragment).navigate(action)
                }
                R.id.profile_menu -> {
                    val action =
                        ViewPagerFragmentDirections.actionViewPagerFragmentToProfileFragment()
                    findNavController(R.id.fragment).navigate(action)
                }
                R.id.log_out -> {
                    auth.signOut()
                    val intent = Intent(this, LogInActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
                }
            }
            true
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (toggle.onOptionsItemSelected(item)) {
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    private fun getData() {
        val header: View = nav_drawer.getHeaderView(0)
        val manIV = header.findViewById<ImageView>(R.id.man_profile_iv)
        val womanIV = header.findViewById<ImageView>(R.id.woman_profile_iv)
        val nameTV = header.findViewById<TextView>(R.id.name_text_on_nav)
        val coverIV = header.findViewById<ImageView>(R.id.cover_image_nav)
        val lastNameTV = header.findViewById<TextView>(R.id.last_name_text_on_nav)
        val professionTV = header.findViewById<TextView>(R.id.prof_text_on_nav)
        val userID = auth.currentUser?.uid
        val getdata = object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {

                val name_string = StringBuilder()
                val last_name = StringBuilder()
                val proff = StringBuilder()
                val gender = StringBuilder()
                for (i in p0.children) {
                    val name = i.child(userID.toString()).child("name").value
                    val lastName = i.child(userID.toString()).child("lastName").value
                    val profession = i.child(userID.toString()).child("prof").value
                    val gndr = i.child(userID.toString()).child("gender").value
                    name_string.append("$name")
                    last_name.append("$lastName")
                    proff.append("$profession")
                    gender.append("$gndr")

                    //gender validation for profile and cover image
                    if (gndr == "Female") {
                        womanIV.visibility = View.VISIBLE
                        Glide.with(baseContext)
                            .load("https://venngage-wordpress.s3.amazonaws.com/uploads/2018/09/Purple-Modern-Pattern-Simple-Background-Image.jpg")
                            .centerCrop()
                            .into(coverIV)
                        nameTV.setTextColor(Color.WHITE)
                        lastNameTV.setTextColor(Color.WHITE)
                        professionTV.setTextColor(Color.WHITE)

                    } else if (gndr == "Male") {
                        manIV.visibility = View.VISIBLE
                        Glide.with(baseContext)
                            .load("https://image.freepik.com/free-vector/red-light-line-futuristic-black-futuristic-background_33869-695.jpg")
                            .centerCrop()
                            .into(coverIV)
                        nameTV.setTextColor(Color.WHITE)
                        lastNameTV.setTextColor(Color.WHITE)
                        professionTV.setTextColor(Color.WHITE)
                    }

                }
                //setting data to text views
                nameTV.text = name_string
                lastNameTV.text = last_name
                professionTV.text = proff
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

        }
        databaseG.addValueEventListener(getdata)
        databaseG.addListenerForSingleValueEvent(getdata)
    }
}