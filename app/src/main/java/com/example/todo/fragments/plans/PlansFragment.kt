package com.example.todo.fragments.plans

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.todo.R
import com.example.todo.data.InputsViewModel
import com.example.todo.fragments.adapters.PlanListAdapter
import kotlinx.android.synthetic.main.fragment_plans.view.*

class PlansFragment : Fragment() {
    private lateinit var mUserInputViewModel: InputsViewModel
    private val args by navArgs<PlansFragmentArgs>()
    private val adapter = PlanListAdapter()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_plans, container, false)


        //recycle view
        val adapter = PlanListAdapter()
        val recycleView = view.recycle_plan
        recycleView.adapter = adapter
        recycleView.layoutManager = LinearLayoutManager(requireContext())
        mUserInputViewModel = ViewModelProvider(this).get(InputsViewModel::class.java)
        mUserInputViewModel.readAllData.observe(viewLifecycleOwner, Observer { inputs ->
            adapter.setData(inputs)
        })
        //swipe delete func
        val ItemTouchHelperCallBack = object : ItemTouchHelper.SimpleCallback(
            0,
            ItemTouchHelper.RIGHT
        ) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return true
            }
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                mUserInputViewModel.delete(adapter.getPlan(viewHolder.adapterPosition))
                Toast.makeText(requireContext(), "deleted", Toast.LENGTH_SHORT).show()
            }
        }
        ItemTouchHelper(ItemTouchHelperCallBack).apply {
            attachToRecyclerView(recycleView)
        }
        view.PlanFloatingActionButton.setOnClickListener {
            findNavController().navigate(R.id.action_viewPagerFragment_to_addPlansFragment2)
        }
        return view
    }
}

