package com.example.todo.fragments.rateapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.example.todo.R
import kotlinx.android.synthetic.main.fragment_rate.*
import kotlinx.android.synthetic.main.fragment_rate.view.*

class RateFragment : DialogFragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_rate, container, false)
        view.radio_group_cancel_bt.setOnClickListener {
            dismiss()
        }

        view.radio_group_submit_bt.setOnClickListener {
            val selectedRBT = radio_group.checkedRadioButtonId
            val radio = view.findViewById<RadioButton>(selectedRBT)
            val rateResult = radio.text.toString()
            if (radio != null) {
                Toast.makeText(requireContext(), "You Rated $rateResult ", Toast.LENGTH_SHORT).show()
                dismiss()
            }else{
                Toast.makeText(requireContext(), "Please Choose one", Toast.LENGTH_SHORT).show()
            }

        }

        return view
    }

}