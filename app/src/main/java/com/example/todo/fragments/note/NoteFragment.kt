package com.example.todo.fragments.note

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.todo.R
import com.example.todo.fragments.adapters.NoteListAdapter
import com.example.todo.notedata.NotesViewModel
import kotlinx.android.synthetic.main.fragment_note.*
import kotlinx.android.synthetic.main.fragment_note.view.*


class NoteFragment : Fragment() {
    private lateinit var mNoteViewModel:NotesViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_note, container, false)
        mNoteViewModel = ViewModelProvider(this).get(NotesViewModel::class.java)
        val adapter = NoteListAdapter()
        val noteRecycle = view.recycle_note
        noteRecycle.adapter = adapter
        noteRecycle.layoutManager = LinearLayoutManager(requireContext())
        mNoteViewModel.readAllNoteData.observe(viewLifecycleOwner, Observer { notes ->
            adapter.setNote(notes)
        })
        val itemTouch = object : ItemTouchHelper.SimpleCallback(
            0,
            ItemTouchHelper.LEFT
        ){
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return true
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                mNoteViewModel.deleteNote(adapter.getNote(viewHolder.adapterPosition))
                Toast.makeText(requireContext(), "deleted", Toast.LENGTH_SHORT).show()
            }

        }
        ItemTouchHelper(itemTouch).apply {
            attachToRecyclerView(recycle_note)
        }

        view.NoteFloatingActionButton.setOnClickListener {
            findNavController().navigate(R.id.action_viewPagerFragment_to_addNoteFragment)
        }
        return view
    }


}