package com.example.todo.fragments.update

import android.os.Bundle
import android.text.TextUtils
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.todo.R
import com.example.todo.data.InputsViewModel
import com.example.todo.data.UserInputs
import kotlinx.android.synthetic.main.fragment_plan_update.*
import kotlinx.android.synthetic.main.fragment_plan_update.view.*

class PlanUpdateFragment : Fragment() {
    private val args by navArgs<PlanUpdateFragmentArgs>()
    lateinit var mPlanViewModel: InputsViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_plan_update, container, false)
        mPlanViewModel = ViewModelProvider(this).get(InputsViewModel::class.java)
        view.plan_update_tv.setText(args.currentPlan.planText)
        view.plan_update_button.setOnClickListener {
            updatePlan()
        }
        setHasOptionsMenu(true)
        return view
    }

    private fun updatePlan() {
        val planInput = plan_update_tv.text.toString()
        if (inputCheck(planInput)) {
            val plans = UserInputs(args.currentPlan.id, planInput)
            mPlanViewModel.updatePlan(plans)
            Toast.makeText(requireContext(), "Successfully updated plan!", Toast.LENGTH_SHORT)
                .show()
            findNavController().navigate(R.id.action_planUpdateFragment_to_viewPagerFragment)
        } else {
            Toast.makeText(requireContext(), "Fill Text Field!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun inputCheck(planInput: String): Boolean {
        return !(TextUtils.isEmpty(planInput))

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.delete_menu,menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menu_delete){
            mPlanViewModel.delete(args.currentPlan)
            Toast.makeText(requireContext(), "Successfully Deleted Your plan!", Toast.LENGTH_SHORT).show()
            findNavController().navigate(R.id.action_planUpdateFragment_to_viewPagerFragment)
        }

        return true
    }

}