package com.example.todo.fragments

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.todo.R
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.view.*

class ProfileFragment : Fragment() {
    private lateinit var auth: FirebaseAuth
    var databaseT = FirebaseDatabase.getInstance().reference
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_profile, container, false)
        getProfileData()
        view.change_password_button.setOnClickListener {
            changePassButton()
        }
        view.apply_change_pass.setOnClickListener {
            changePassword()
        }
        return view
    }

    private fun changePassButton() {
        password_change_card.visibility = View.VISIBLE
        change_password_button.visibility = View.GONE
    }

    private fun getProfileData() {
        auth = Firebase.auth
        val userID = auth.currentUser?.uid
        val getdata = object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                val name = StringBuilder()
                val lastName = StringBuilder()
                val profession = StringBuilder()
                val email = StringBuilder()
                val mobileNum = StringBuilder()
                val password = StringBuilder()
                for (i in p0.children) {
                    val nm = i.child(userID.toString()).child("name").value
                    val ln = i.child(userID.toString()).child("lastName").value
                    val pr = i.child(userID.toString()).child("prof").value
                    val pass = i.child(userID.toString()).child("pass").value
                    val em = i.child(userID.toString()).child("email").value
                    val mn = i.child(userID.toString()).child("mobile").value
                    val gndr = i.child(userID.toString()).child("gender").value
                    name.append("$nm")
                    lastName.append("$ln")
                    profession.append("$pr")
                    email.append("$em")
                    mobileNum.append("$mn")
                    password.append("$pass")
                    if (gndr == "Female") {
                        profile_womanIV.visibility = View.VISIBLE
                        Glide.with(context!!)
                            .load("https://venngage-wordpress.s3.amazonaws.com/uploads/2018/09/Purple-Modern-Pattern-Simple-Background-Image.jpg")
                            .centerCrop()
                            .into(profile_act_cover)
                        profile_act_nameTV.setTextColor(Color.WHITE)
                        profile_act_last_nameTV.setTextColor(Color.WHITE)
                        profile_act_professionTV.setTextColor(Color.WHITE)

                    } else if (gndr == "Male") {
                        profile_manIV.visibility = View.VISIBLE
                        Glide.with(context!!)
                            .load("https://image.freepik.com/free-vector/red-light-line-futuristic-black-futuristic-background_33869-695.jpg")
                            .centerCrop()
                            .into(profile_act_cover)
                        profile_act_nameTV.setTextColor(Color.BLACK)
                        profile_act_last_nameTV.setTextColor(Color.BLACK)
                        profile_act_professionTV.setTextColor(Color.BLUE)
                    }
                }
                profile_act_passwordTV.text = password
                profile_act_nameTV.text = name
                profile_act_professionTV.text = profession
                profile_act_last_nameTV.text = lastName
                profile_act_emailTV.text = email
                profile_act_mobTV.text = mobileNum
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

        }
        databaseT.addValueEventListener(getdata)
        databaseT.addListenerForSingleValueEvent(getdata)

    }

    private fun changePassword() {
        auth = Firebase.auth
        if (change_password_currentpass.text.toString().isNotEmpty() &&
            change_password_newPass.text.toString().isNotEmpty() &&
            change_password_confPas.text.toString().isNotEmpty()
        ) {

            if (change_password_newPass.text.toString() == change_password_confPas.text.toString()) {

                val user = auth.currentUser
                if (user != null && user.email != null) {
                    val credential = EmailAuthProvider
                        .getCredential(user.email!!, change_password_currentpass.text.toString())
                    user.reauthenticate(credential)
                        .addOnCompleteListener {
                            if (it.isSuccessful) {
                                Toast.makeText(
                                    context,
                                    "Re-Authentication success.",
                                    Toast.LENGTH_SHORT
                                ).show()
                                user.updatePassword(change_password_newPass.text.toString())
                                    .addOnCompleteListener { task ->
                                        if (task.isSuccessful) {
                                            val newPassword =
                                                change_password_confPas.text.toString()
                                            val userID = auth.currentUser?.uid
                                            Toast.makeText(
                                                context,
                                                "Password changed successfully.",
                                                Toast.LENGTH_SHORT
                                            ).show()
                                            databaseT.child("User").child(userID.toString())
                                                .child("pass").setValue(newPassword)
                                            password_change_card.visibility = View.GONE
                                            change_password_button.visibility = View.VISIBLE
                                        }
                                    }

                            } else {
                                Toast.makeText(
                                    context,
                                    "Re-Authentication failed.",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                } else {
                    Toast.makeText(context, "some error", Toast.LENGTH_SHORT).show()
                }

            } else {
                Toast.makeText(context, "Please repeat new password.", Toast.LENGTH_SHORT).show()
            }

        } else {
            Toast.makeText(context, "Please enter all the fields.", Toast.LENGTH_SHORT).show()
        }

    }
}