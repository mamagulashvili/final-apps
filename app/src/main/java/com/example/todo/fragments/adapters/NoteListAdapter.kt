package com.example.todo.fragments.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.todo.R
import com.example.todo.data.UserInputs
import com.example.todo.fragments.update.NotesUpdateFragmentDirections
import com.example.todo.notedata.UserNotes
import com.example.todo.viewPager.ViewPagerFragmentDirections
import kotlinx.android.synthetic.main.note_row.view.*

class NoteListAdapter:RecyclerView.Adapter<NoteListAdapter.NoteViewHolder>() {
    private var noteList = emptyList<UserNotes>()

    inner class NoteViewHolder(itemView: View):RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.note_row,parent,false)
        return NoteViewHolder(v)
    }

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        val currentNote = noteList[position]
        holder.itemView.note_tv.text = currentNote.notes
        holder.itemView.note_edit_image_bt.setOnClickListener {
           val action = ViewPagerFragmentDirections.actionViewPagerFragmentToNotesUpdateFragment(currentNote)
                holder.itemView.findNavController().navigate(action)
        }
    }

    override fun getItemCount(): Int {
        return noteList.size
    }
    fun setNote(notes:List<UserNotes>){
        this.noteList = notes
        notifyDataSetChanged()
    }
    fun getNote(position: Int): UserNotes {
        return noteList[position]
    }
}