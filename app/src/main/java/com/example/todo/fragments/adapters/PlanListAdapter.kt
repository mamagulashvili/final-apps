package com.example.todo.fragments.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.todo.R
import com.example.todo.data.InputsRepository
import com.example.todo.data.InputsViewModel
import com.example.todo.data.UserInputs
import com.example.todo.viewPager.ViewPagerFragmentDirections
import kotlinx.android.synthetic.main.plan_row.view.*

class PlanListAdapter : RecyclerView.Adapter<PlanListAdapter.PlansViewHolder>() {
    private var planInputLits = emptyList<UserInputs>()
    private lateinit var mUserInputViewModel: InputsViewModel
    private lateinit var repository: InputsRepository


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlansViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.plan_row, parent, false)
        return PlansViewHolder(v)
    }

    override fun onBindViewHolder(holder: PlansViewHolder, position: Int) {
        val currentItem = planInputLits[position]
        holder.itemView.planRow_tv.text = currentItem.planText
        holder.itemView.plan_edit_bt.setOnClickListener {
            val action = ViewPagerFragmentDirections.actionViewPagerFragmentToPlanUpdateFragment(currentItem)
            holder.itemView.findNavController().navigate(action)
        }
    }

    override fun getItemCount(): Int {
        return planInputLits.size
    }

    fun setData(input: List<UserInputs>) {
        this.planInputLits = input
        notifyDataSetChanged()
    }

    fun getPlan(position: Int): UserInputs {
        return planInputLits[position]
    }

    inner class PlansViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}