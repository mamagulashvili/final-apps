package com.example.todo.fragments.plans

import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.todo.R
import com.example.todo.data.InputsViewModel
import com.example.todo.data.UserInputs
import kotlinx.android.synthetic.main.fragment_add_plans.*
import kotlinx.android.synthetic.main.fragment_add_plans.view.*

class AddPlansFragment : Fragment() {
    private lateinit var mInputViewModel:InputsViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view= inflater.inflate(R.layout.fragment_add_plans, container, false)

        mInputViewModel = ViewModelProvider(this).get(InputsViewModel::class.java )
        view.plans_add_button.setOnClickListener {
            insertPlanData()
        }
        return view
    }
    private fun insertPlanData(){
        val planInput = plans_input_et.text.toString()
        if (inputCheck(planInput)){
            val input = UserInputs(0,planInput)
            mInputViewModel.addInputText(input)
            Toast.makeText(requireContext(), "Successfully added Daily Plan!! ", Toast.LENGTH_SHORT).show()
            findNavController().navigate(R.id.action_addPlansFragment2_to_viewPagerFragment)
        }else{
            Toast.makeText(requireContext(), "please fill this field!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun inputCheck(planInput:String):Boolean{
        return !(TextUtils.isEmpty(planInput))


    }

}