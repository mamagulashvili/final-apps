package com.example.todo.fragments.note

import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.todo.R
import com.example.todo.notedata.NotesViewModel
import com.example.todo.notedata.UserNotes
import kotlinx.android.synthetic.main.fragment_add_note.*
import kotlinx.android.synthetic.main.fragment_add_note.view.*

 class AddNoteFragment : Fragment() {
    private lateinit var mNoteViewModel:NotesViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_add_note, container, false)
        mNoteViewModel = ViewModelProvider(this).get(NotesViewModel::class.java)


        view.notes_add_button.setOnClickListener {
            insertNotesData()
            findNavController().navigate(R.id.action_addNoteFragment_to_viewPagerFragment)
        }

        return view
    }
    private fun insertNotesData(){
        val noteInput = notes_input_et.text.toString()
        if (noteChecker(noteInput)){
            val noteInputs = UserNotes(0,noteInput)
            mNoteViewModel.addNotes(noteInputs)
            Toast.makeText(requireContext(), "Successfully added Ur Note!", Toast.LENGTH_SHORT).show()
        }else{
            Toast.makeText(requireContext(), "Failed", Toast.LENGTH_SHORT).show()
        }
    }


    private fun noteChecker(noteInput:String):Boolean{
        return !(TextUtils.isEmpty(noteInput))
    }

}