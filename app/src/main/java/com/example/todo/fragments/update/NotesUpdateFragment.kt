package com.example.todo.fragments.update

import android.app.AlertDialog
import android.os.Bundle
import android.text.TextUtils
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.todo.R
import com.example.todo.notedata.NotesViewModel
import com.example.todo.notedata.UserNotes
import kotlinx.android.synthetic.main.fragment_notes_update.*
import kotlinx.android.synthetic.main.fragment_notes_update.view.*

class NotesUpdateFragment : Fragment() {
    private val args by navArgs<NotesUpdateFragmentArgs>()
    private lateinit var mNoteViewModel: NotesViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_notes_update, container, false)
        mNoteViewModel = ViewModelProvider(this).get(NotesViewModel::class.java)

        view.note_update_tv.setText(args.currentNote.notes)
        view.note_update_bt.setOnClickListener {
            updateNote()
        }

        setHasOptionsMenu(true)
        return view
    }

    private fun updateNote() {
        val noteInput = note_update_tv.text.toString()
        if (noteChecker(noteInput)) {
            val updateNote = UserNotes(args.currentNote.id, noteInput)
            mNoteViewModel.updateNote(updateNote)
            Toast.makeText(requireContext(), "updated", Toast.LENGTH_SHORT).show()
            findNavController().navigate(R.id.action_notesUpdateFragment_to_viewPagerFragment)
        }
    }

    private fun noteChecker(noteInput: String): Boolean {
        return !(TextUtils.isEmpty(noteInput))
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.delete_menu,menu)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menu_delete){
            mNoteViewModel.deleteNote(args.currentNote)
            Toast.makeText(requireContext(), "Successfully Deleted", Toast.LENGTH_SHORT).show()
            findNavController().navigate(R.id.action_notesUpdateFragment_to_viewPagerFragment)
        }
        return true
    }
    private fun deleteNote(){
        val builder = AlertDialog.Builder(requireContext())
        builder.setPositiveButton("Yes"){_,_ ->
            mNoteViewModel.deleteNote(args.currentNote)
            Toast.makeText(requireContext(), "Successfully Deleted", Toast.LENGTH_SHORT).show()
            findNavController().navigate(R.id.action_notesUpdateFragment_to_viewPagerFragment)
        }
        builder.setNegativeButton("No"){_,_ ->
            Toast.makeText(requireContext(), "some error", Toast.LENGTH_SHORT).show()
        }
        builder.setTitle("Delete Note")
        builder.setMessage("Are you sure you want to delete this note?")
    }
}