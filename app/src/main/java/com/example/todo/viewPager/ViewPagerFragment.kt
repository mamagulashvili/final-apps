package com.example.todo.viewPager

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.todo.R
import com.example.todo.data.InputsViewModel
import com.example.todo.fragments.note.NoteFragment
import com.example.todo.fragments.plans.PlansFragment
import com.example.todo.fragments.rateapp.RateFragment
import com.example.todo.notedata.NotesViewModel
import kotlinx.android.synthetic.main.fragment_view_pager.view.*

class ViewPagerFragment : Fragment() {
    private lateinit var mNotesViewModel: NotesViewModel
    private lateinit var mPlanViewModel: InputsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_view_pager, container, false)
        mNotesViewModel = ViewModelProvider(this).get(NotesViewModel::class.java)
        mPlanViewModel = ViewModelProvider(this).get(InputsViewModel::class.java)
        val adapter = ViewPagerAdapter(childFragmentManager)
        adapter.addFragment(PlansFragment(), "Plans")
        adapter.addFragment(NoteFragment(), "Notes")
        view.viewPager.adapter = adapter
        val tabs = view.tab
        tabs.setupWithViewPager(view.viewPager)
        view.tab.getTabAt(0)!!.setIcon(R.drawable.ic_notebook).text = "Plans"
        view.tab.getTabAt(1)!!.setIcon(R.drawable.ic_note).text = "Note"

        setHasOptionsMenu(true)
        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.view_pager_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val dialog = RateFragment()
        when (item.itemId) {
            R.id.del_all_note -> mNotesViewModel.deleteAllNotes()
            R.id.del_all_plan -> mPlanViewModel.deleteAllPlan()
            R.id.menu_rate_app -> openDialog()
        }

        return true
    }

    private fun openDialog() {
        val dialog = RateFragment()
        dialog.show(childFragmentManager, "RateDialog")
    }

}