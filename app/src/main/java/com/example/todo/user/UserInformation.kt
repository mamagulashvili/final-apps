package com.example.todo.user

class UserInformation(
    var name: String,
    var lastName: String,
    var prof: String,
    var email: String,
    var mobile: String,
    var pass: String,
    var gender:String
)
