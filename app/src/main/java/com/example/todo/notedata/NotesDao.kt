package com.example.todo.notedata

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface NotesDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addNote(notes: UserNotes)

    @Query("SELECT * FROM notes_table ORDER BY id ASC")
    fun readAllNoteData(): LiveData<List<UserNotes>>
    @Update
    suspend fun updateNote(notes: UserNotes)
    @Delete
    suspend fun deleteNote(notes: UserNotes)
    @Query("DELETE FROM notes_table")
    suspend fun deleteAllNote()
}