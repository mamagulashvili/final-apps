package com.example.todo.notedata

import androidx.lifecycle.LiveData

class NotesRepository(private val notesDao: NotesDao) {
    val readAllNoteData:LiveData<List<UserNotes>> = notesDao.readAllNoteData()
    suspend fun addNote(notes: UserNotes){
        notesDao.addNote(notes)
    }
    suspend fun deleteNote(notes: UserNotes){
        notesDao.deleteNote(notes)
    }
    suspend fun updateNote(notes: UserNotes){
        notesDao.updateNote(notes)
    }
    suspend fun deleteAllNote(){
        notesDao.deleteAllNote()
    }
}