package com.example.todo.notedata

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NotesViewModel(application: Application) : AndroidViewModel(application) {
    val readAllNoteData:LiveData<List<UserNotes>>
    private val repository:NotesRepository
    init {
        val notesDao = NotesDatabase.getNoteDatabase(application).notesDao()
        repository = NotesRepository(notesDao)
        readAllNoteData = repository.readAllNoteData
    }

    fun addNotes(notes: UserNotes){
        viewModelScope.launch (Dispatchers.IO){
            repository.addNote(notes)
        }
    }
    fun  deleteNote(notes: UserNotes){
        viewModelScope.launch (Dispatchers.IO){
            repository.deleteNote(notes)
        }
    }
    fun updateNote(notes: UserNotes){
        viewModelScope.launch(Dispatchers.IO) {
            repository.updateNote(notes)
        }
    }
    fun deleteAllNotes(){
        viewModelScope.launch (Dispatchers.IO){
            repository.deleteAllNote()
        }
    }
}