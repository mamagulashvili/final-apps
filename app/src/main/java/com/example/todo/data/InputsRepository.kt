package com.example.todo.data

import androidx.lifecycle.LiveData

class InputsRepository(private val inputsDao: InputDao) {
    val readAllData: LiveData<List<UserInputs>> = inputsDao.readAllData()
    suspend fun addInputText(inputs: UserInputs) {
        inputsDao.addInputText(inputs)
    }
     fun deletePlan(inputs: UserInputs){
        inputsDao.deletePlanInput(inputs)
    }
    suspend fun updatePlan(inputs: UserInputs){
        inputsDao.updatePlan(inputs)
    }
    suspend fun deleteAllPlan(){
        inputsDao.deleteAllPlan()
    }
}