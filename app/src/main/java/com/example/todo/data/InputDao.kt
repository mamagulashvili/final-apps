package com.example.todo.data

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface InputDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addInputText(input: UserInputs)

    @Query("SELECT * FROM user_inputs ORDER BY id ASC")
    fun readAllData(): LiveData<List<UserInputs>>
    @Update
    suspend fun updatePlan(input: UserInputs)

    @Delete
    fun deletePlanInput(input: UserInputs)
    @Query("DELETE FROM user_inputs")
    suspend fun deleteAllPlan()
}
