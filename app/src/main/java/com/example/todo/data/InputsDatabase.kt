package com.example.todo.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(
    entities = [UserInputs::class],
    version = 1,
    exportSchema = false
)
abstract class InputsDatabase : RoomDatabase() {

    abstract fun inputsDao(): InputDao

    companion object {
        @Volatile
        private var INSTANCE: InputsDatabase? = null
        fun getDatabase(context: Context) : InputsDatabase{
            val tempInstance = INSTANCE
            if (tempInstance != null){
                return tempInstance
            }
            synchronized(this){
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    InputsDatabase::class.java,
                    "inputs_database"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }

}