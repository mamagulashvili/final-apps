package com.example.todo.data

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class InputsViewModel(application: Application):AndroidViewModel(application) {
     val readAllData:LiveData<List<UserInputs>>
    private val repository: InputsRepository
    init {
        val inputsDao = InputsDatabase.getDatabase(application).inputsDao()
        repository = InputsRepository(inputsDao)
        readAllData = repository.readAllData
    }

    fun addInputText(inputs: UserInputs){
        viewModelScope.launch (Dispatchers.IO){
            repository.addInputText(inputs)
        }
    }
    fun delete(inputs: UserInputs){
        viewModelScope.launch (Dispatchers.IO){
            repository.deletePlan(inputs)
        }
    }
    fun updatePlan(inputs: UserInputs){
        viewModelScope.launch(Dispatchers.IO) {
            repository.updatePlan(inputs)
        }
    }
    fun deleteAllPlan(){
        viewModelScope.launch (Dispatchers.IO){
            repository.deleteAllPlan()
        }
    }
}