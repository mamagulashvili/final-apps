package com.example.todo.data

import android.os.Parcel
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "user_inputs")
data class UserInputs(
    @PrimaryKey(autoGenerate = true)
    val id :Int,
    val planText:String,
):Parcelable